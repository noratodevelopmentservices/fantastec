import React, {useEffect, useRef} from 'react';
import {FlatList, View} from 'react-native';
import {LiveCommentaryItemComponent} from '../liveCommentaryItem';
import {Commentary} from '@entities';

interface LiveCommentaryComponentProps {
  commentary: Commentary[];
  selectedCommentary?: string;
}

export const LiveCommentaryComponent: React.FC<LiveCommentaryComponentProps> = ({
  commentary,
  selectedCommentary,
}) => {
  const flatListRef = useRef<any>();
  const renderItem = ({item, index}: {item: Commentary; index: number}) => {
    return (
      <LiveCommentaryItemComponent
        {...item}
        isSelected={item.id === selectedCommentary}
        isLast={index === commentary.length - 1}
      />
    );
  };

  useEffect(() => {
    if (selectedCommentary !== undefined) {
      const index = commentary.findIndex(
        (comment: Commentary) => comment.id === selectedCommentary,
      );
      if (flatListRef.current) {
        flatListRef.current.scrollToIndex({animated: true, index});
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedCommentary]);

  return (
    <>
      <FlatList
        ref={flatListRef}
        scrollEventThrottle={1}
        ListHeaderComponent={<View style={{height: 20}} />}
        data={commentary}
        renderItem={renderItem}
        keyExtractor={(item: Commentary) => item.id}
        showsVerticalScrollIndicator={false}
      />
    </>
  );
};
