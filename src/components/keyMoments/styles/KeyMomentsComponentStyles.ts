import Styled from 'styled-components/native';
import {isIos} from '../../../core/utils';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  mainContainer: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    backgroundColor: 'white',
    paddingTop: 10,
    paddingBottom: isIos ? 40 : 70,
    paddingHorizontal: 20,
  },
});

export const KeyMomentsTitleLabel = Styled.Text`
	align-self: center;
	font-size: 24px;
	margin-bottom: 20px;
`;
