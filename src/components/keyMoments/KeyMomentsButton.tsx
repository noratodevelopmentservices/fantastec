import {TouchableOpacity, ViewStyle} from 'react-native';
import React from 'react';

interface KeyMomentsButtonProps {
  setAnimation: (func: (prevState: string) => string) => void;
  setPaddingBottom: (paddingBottom: number) => void;
  animation: string;
  style?: ViewStyle;
}

export const KeyMomentsButton: React.FC<KeyMomentsButtonProps> = ({
  children,
  setAnimation,
  setPaddingBottom,
  animation,
  style,
}) => {
  return (
    <TouchableOpacity
      style={style}
      onPress={() => {
        setAnimation((prevState: string) =>
          prevState === 'out' ? 'in' : 'out',
        );
        setPaddingBottom(animation === 'out' ? 180 : 40);
      }}>
      {children}
    </TouchableOpacity>
  );
};
