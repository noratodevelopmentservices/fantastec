import React, {memo, useState, useEffect} from 'react';
import {KeyMomentsTitleLabel, styles} from './styles/KeyMomentsComponentStyles';
import {Text, TouchableOpacity, Animated} from 'react-native';
import {Commentary} from '@entities';
import Icon from 'react-native-vector-icons/FontAwesome';
import {isIos} from '../../core/utils';
import {KeyMomentsButton} from './KeyMomentsButton';

interface KeyMomentsComponentProps {
  commentary: Commentary[];
  setSelectedCommentary: (selectedCommentary: string) => void;
  setPaddingBottom: (paddingBottom: number) => void;
}

const keyMomentsLookupTable: {[key: string]: string} = {
  yellow: 'Yellow Card',
  red: 'Red Card',
  goal: 'Goal!',
  save: 'Save',
};

const KeyMomentsComponent: React.FC<KeyMomentsComponentProps> = ({
  commentary,
  setSelectedCommentary,
  setPaddingBottom,
}) => {
  const y = new Animated.Value(0);
  y.addListener(({value}) => console.log(value));
  const [animation, setAnimation] = useState('in');
  const anim = (toValue: number) =>
    Animated.timing(y, {
      toValue,
      duration: 500,
      useNativeDriver: true,
    });

  const [isFirstTimeRendering, setIsFirstTimeRendering] = useState(true);

  useEffect(() => {
    if (!isFirstTimeRendering) {
      if (animation === 'out') {
        anim(isIos ? 130 : 140).start();
      } else {
        anim(0).start();
      }
    } else {
      setIsFirstTimeRendering(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [animation]);
  return (
    <>
      <Animated.View
        style={[styles.mainContainer, {transform: [{translateY: y}]}]}>
        <KeyMomentsButton
          animation={animation}
          setPaddingBottom={setPaddingBottom}
          setAnimation={setAnimation}>
          <KeyMomentsTitleLabel>Key moments</KeyMomentsTitleLabel>
        </KeyMomentsButton>
        {commentary.map((comment) => {
          return (
            <TouchableOpacity
              key={comment.id}
              onPress={() => setSelectedCommentary(comment.id)}
              style={{flexDirection: 'row', margin: 5}}>
              <Text style={{marginRight: 20}}>{`${comment.minute}'`}</Text>
              <Text>{keyMomentsLookupTable[comment.type]}</Text>
            </TouchableOpacity>
          );
        })}
        <KeyMomentsButton
          animation={animation}
          setPaddingBottom={setPaddingBottom}
          setAnimation={setAnimation}
          style={{
            position: 'absolute',
            right: 20,
            top: 15,
          }}>
          <Icon
            name={animation === 'out' ? 'chevron-up' : 'chevron-down'}
            size={21}
            color={'black'}
          />
        </KeyMomentsButton>
      </Animated.View>
    </>
  );
};

export const KeyMoments = memo(KeyMomentsComponent);
