import Styled from 'styled-components/native';

export const Wrapper = Styled.View`
	background-color: white;
	elevation: 7;
	padding-vertical: 10px;
	width: 100%;
	shadow-offset: 0px 1px;
	shadow-color: #c4c4c4;
	shadow-radius: 1px;
	shadow-opacity: 1;
	margin-bottom: 2px;
`;

export const TitleLabel = Styled.Text`
	font-size: 24px;
	align-self: center;
`;
