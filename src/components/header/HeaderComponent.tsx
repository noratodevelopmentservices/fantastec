import React from 'react';
import {Wrapper, TitleLabel} from './styles/HeaderComponentStyles';

export const HeaderComponent: React.FC = () => {
  return (
    <Wrapper>
      <TitleLabel>Live Commentary</TitleLabel>
    </Wrapper>
  );
};
