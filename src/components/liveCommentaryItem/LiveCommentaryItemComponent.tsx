import React from 'react';
import {View} from 'react-native';
import {TimelineCircleComponent} from '../timelineCircle';
import {
  styles,
  DescriptionLabel,
  HighlightedDescriptionWrapper,
  DescriptionWrapper,
} from './styles/LiveCommentaryItemComponentStyles';
import {Commentary} from '@entities';

interface LiveCommentaryItemProps extends Commentary {
  isLast: boolean;
  isSelected: boolean;
}

const {mainContainer, timelineContainer} = styles;

export const LiveCommentaryItemComponent: React.FC<LiveCommentaryItemProps> = ({
  description,
  type,
  isLast = false,
  minute,
  isSelected,
}) => {
  return (
    <View style={mainContainer}>
      <View style={timelineContainer}>
        <TimelineCircleComponent
          minute={minute}
          connectToNextCircle={!isLast}
          type={type}
          isHighlighted={isSelected}
        />
      </View>
      {isSelected && (
        <HighlightedDescriptionWrapper type={type}>
          <DescriptionLabel>{description}</DescriptionLabel>
        </HighlightedDescriptionWrapper>
      )}
      {!isSelected && (
        <DescriptionWrapper>
          <DescriptionLabel>{description}</DescriptionLabel>
        </DescriptionWrapper>
      )}
    </View>
  );
};
