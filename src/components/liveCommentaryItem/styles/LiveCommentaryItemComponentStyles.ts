import styled from 'styled-components/native';
import {CommentaryType} from '../../../entities/Commentary';
import {getBorderColor} from '../../../core/utils';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  mainContainer: {
    flexDirection: 'row',
    flex: 1,
    marginBottom: 40,
  },
  timelineContainer: {
    alignItems: 'center',
    marginRight: 20,
  },
});

export const TimelineWrapper = styled.View`
  align-items: center;
  margin-right: 20px;
`;

export const HighlightedDescriptionWrapper = styled.View`
  flex: 2;
  border-top-left-radius: 18px;
  border-bottom-right-radius: 18px;
  border-color: ${({type}: {type: CommentaryType}) => getBorderColor(type)};
  border-width: ${({type}: {type: CommentaryType}) =>
    type !== 'action' ? 2 : 1}px;
  padding: 10px;
  background-color: white;
`;

export const DescriptionWrapper = styled.View`
  width: 75%;
  border-top-left-radius: 18px;
  border-bottom-right-radius: 18px;
  border-color: grey;
  border-width: 1px;
  padding: 10px;
  background-color: white;
`;

export const DescriptionLabel = styled.Text`
  font-weight: 200;
  font-size: 18px;
`;
