import React from 'react';
import {View, Text} from 'react-native';
import {CommentaryType} from '@entities';
import {getBorderColor} from '../../core/utils';

interface TimelineCircleComponentProps {
  minute: number;
  connectToNextCircle: boolean;
  type: CommentaryType;
  isHighlighted: boolean;
}

export const TimelineCircleComponent: React.FC<TimelineCircleComponentProps> = ({
  minute,
  connectToNextCircle,
  type,
  isHighlighted,
}) => {
  return (
    <>
      <View
        style={{
          width: 60,
          height: 60,
          borderRadius: 30,
          borderWidth: type !== 'action' && isHighlighted ? 3 : 1,
          borderColor: isHighlighted ? getBorderColor(type) : 'grey',
          backgroundColor: 'white',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text style={{fontSize: 29, fontWeight: '100'}}>{`${minute}'`}</Text>
      </View>
      {connectToNextCircle && (
        <View style={{width: 1, height: '100%', backgroundColor: 'grey'}} />
      )}
    </>
  );
};
