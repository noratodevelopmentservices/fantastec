import {Response, Commentary} from '@entities';

export interface MatchServiceContract {
  getCommentary: () => Promise<Response<Commentary[]>>;
}
