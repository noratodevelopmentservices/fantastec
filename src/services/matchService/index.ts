import {MockMatchService} from './MockMatchService';
import {MatchService} from './MatchService';
import config from '../../core/config';
import {MatchServiceContract} from '../contracts/MatchServiceContract';

const serviceObject: {matchService: MatchServiceContract} = {
  matchService: config.debug ? MockMatchService : MatchService,
};

export default serviceObject;
