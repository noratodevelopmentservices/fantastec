import {MatchServiceContract} from '../contracts/MatchServiceContract';

const mockCommentary = require('../../mockData/commentary.json');

const API_DELAY = 1500;

export const MockMatchService: MatchServiceContract = {
  getCommentary: () => {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({success: true, status: 201, data: mockCommentary});
      }, API_DELAY);
    });
  },
};
