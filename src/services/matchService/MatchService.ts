import {MatchServiceContract} from '../contracts/MatchServiceContract';
import axios from 'axios';

export const MatchService: MatchServiceContract = {
  getCommentary: () => {
    return axios.get('PATH_COMMENTARY_API');
  },
};
