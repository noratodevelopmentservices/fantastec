import {CommentaryType} from '../entities/Commentary';
import {Platform} from 'react-native';

export const getBorderColor = (type: CommentaryType) => {
  const lookupTable = {
    yellow: '#F7A704',
    red: 'red',
    goal: '#5AC171',
    save: '#A27EFF',
    action: 'grey',
  };
  return lookupTable[type];
};

export const isIos = Platform.OS === 'ios';
