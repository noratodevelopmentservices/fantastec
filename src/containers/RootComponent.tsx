import React from 'react';
import {SafeAreaView} from 'react-native';
import {HomeScreen} from '@containers';

const RootComponent = () => {
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
      <HomeScreen />
    </SafeAreaView>
  );
};

export default RootComponent;
