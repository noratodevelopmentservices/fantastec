import {Platform} from 'react-native';
import Styled from 'styled-components/native';

const isIos = Platform.OS === 'ios';

export const CommentaryWrapper = Styled.View`
	background-color: #f4f4f4;
	padding-horizontal: 25px;
	padding-bottom: ${isIos ? 10 : 40}px;
`;

export const EmptyCommentaryWrapper = Styled.View`
	flex: 1;
	align-items: center;
	justify-content: center;
`;
