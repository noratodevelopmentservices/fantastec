import React, {useEffect, useState} from 'react';
import {
  LiveCommentaryComponent,
  HeaderComponent,
  KeyMoments,
} from '@components';
import MatchService from '../../services/matchService';
import {ActivityIndicator, View, Text} from 'react-native';
import {Commentary} from '@entities';
import {
  EmptyCommentaryWrapper,
  CommentaryWrapper,
} from './styles/HomeScreenStyles';

export const HomeScreen: React.FC = () => {
  const [paddingBottom, setPaddingBottom] = useState(180);
  const [selectedCommentary, setSelectedCommentary] = useState<
    string | undefined
  >(undefined);
  const {matchService} = MatchService;
  const [commentary, setCommentary] = useState<Commentary[]>([]);
  const onKeyMomentPressed = (id: string) => {
    setSelectedCommentary(id);
  };
  useEffect(() => {
    const getCommentary = async () => {
      try {
        const commentaryResponse = await matchService.getCommentary();
        if (commentaryResponse.success) {
          setCommentary(commentaryResponse.data!);
        }
      } catch (e) {
        // TODO implement error management
        console.log(e);
      }
    };
    getCommentary();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const emptyCommentary = () => {
    return (
      <EmptyCommentaryWrapper>
        <Text style={{alignSelf: 'center'}}>Loading...</Text>
        <ActivityIndicator size="large" color="#000" />
      </EmptyCommentaryWrapper>
    );
  };
  if (commentary.length === 0) {
    return emptyCommentary();
  }
  return (
    <>
      <HeaderComponent />
      {commentary.length > 1 && (
        <View style={{justifyContent: 'space-between', paddingBottom}}>
          <CommentaryWrapper>
            <LiveCommentaryComponent
              commentary={commentary}
              selectedCommentary={selectedCommentary}
            />
          </CommentaryWrapper>
          <KeyMoments
            commentary={commentary.filter((comment) => {
              return /yellow|red|save|goal/.test(comment.type);
            })}
            setSelectedCommentary={onKeyMomentPressed}
            setPaddingBottom={setPaddingBottom}
          />
        </View>
      )}
    </>
  );
};
