import {Error} from './Error';

export interface Response<DataType> {
  success: boolean;
  status: number;
  error?: Error;
  data?: DataType;
}
