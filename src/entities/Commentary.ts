export interface Commentary {
  id: string;
  minute: number;
  description: string;
  type: CommentaryType;
}

export type CommentaryType = 'action' | 'save' | 'goal' | 'yellow' | 'red';
